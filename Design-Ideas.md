# Design Ideas

A space to brainstorm design ideas for the camera design.  Here's some starting areas for key design considerations:

* How to bring down the cost?  How to create a variety of masks for the camera quickly?
* Where are and what are the algorithms for recreating the image?
* List a variety of detectors (hamamatsu mini-spec is an example http://www.hamamatsu.com/us/en/product/category/5001/4016/C12880MA/index.html) 
* We'll need display software and analysis software solutions - if successful, there's a lot of data and interpretation that needs to happen
* How can we modularize the system, so collaboration becomes easier (people can focus on their parts without breaking others)
...
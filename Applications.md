# Applications

Put ideas for applications of the output of such a device here

## Hyperspectral video for environmental monitoring

Hyperspectral video cameras are currently very expensive (http://cubert-gmbh.com/product/uhd-485-lion/) and therefore uses are currently limited.  But a lower cost, or DIY version could dramtaically increase the application space... 
... add specific papers showing hyperspectral camera's use in environmental sensing (water quality, smokestack pollutant detection, ... ) 
... 

## High dynamic range/high sensitivity (e.g. for fluorescence)

Using a super sensitive detector (e.g. [SPAD](https://en.wikipedia.org/wiki/Single-photon_avalanche_diode) or [PMT](https://en.wikipedia.org/wiki/Photomultiplier)) with a single-pixel camera potentially allows us to reconstruct an image with higher dynamic range, or better sensitivity (by which I probably mean lower "dark noise") than typical CMOS cameras.  That would be really nice for looking at weak fluorescence signals, for example.

## Structured illumination microscopy

The single-pixel-camera approach can be modified slightly, so that there's a double-pass through the mask.  This can give some of the benefits of confocal microscopy, but in a potentially much cheaper system.

## Non-visible imaging

Beyond about 1000nm, Si detectors no longer work - it's hard to build cameras out of e.g. GaAs, but buying photodiodes is relatively easy.  People have used this to make thermal imaging cameras.